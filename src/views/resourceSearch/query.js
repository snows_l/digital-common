import { defineComponent, reactive, ref, watch,computed } from 'vue';
import api from '@/api/homePage'

export default function queryHandle(initTable,modelCode){
    let activeKey=ref('')
    let tabList=reactive({
        list:[]
    })
    let typeList=reactive({
        list:[]
    })

    function getTypeList(){
        api.findTypeByGroup()
        .then(({data})=>{
            let tabCode=null
            tabList.list=data.map(t=>{
                const tempList= t.data.filter(t=>t.code===modelCode)
                if(tempList.length>0){
                    tabCode=t.group
                }
                return{
                    name:t.group,
                    code:t.group,
                    child:t.data
                }
            })
            if(tabList.list.length>0){
                if(tabCode){
                    changeTab(tabCode)
                }else{
                    changeTab(tabList.list[0].code)
                }
            }
        })
    }
    getTypeList()

    function changeTab(code,isHand=false){
        activeKey.value=code
        const activeList = tabList.list.filter(t=>t.code===code)
        if(activeList.length>0){
            typeList.list= activeList[0].child
        }else{
            typeList.list= []
        }
        if(typeList.list.length>0){
            if(modelCode && !isHand){
                changeType({code:modelCode})
            }else{
                changeType(typeList.list[0])
            }
        }
    }
    let currentType=ref("")
    function changeType(type){
        currentType.value=type.code
        getQueryFieldList(type.code)
    }


    let queryFields=reactive({
        list:[]
    })
    let showAttr=reactive({
        list:[]
    })
    function getQueryFieldList(code){
        api.findAllAttr(code).then(res=>{
            queryFields.list=res.data
            showAttr.list=res.data.slice(0,6)
            if(initTable){
                initTable()
            }
        })
    }


    return {
        activeKey,
        tabList,
        changeTab,

        currentType,
        typeList,
        changeType,

        showAttr,
        queryFields
    }
}