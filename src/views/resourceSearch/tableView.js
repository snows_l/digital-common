const columns=[{
    key: 'name',
    dataIndex:"name",
    title: '业务名称'
},{
    key: 'belongDepartment',
    dataIndex:"belongDepartment",
    title: '业务名称模块'
},{
    key: 'tenantName',
    dataIndex:"tenantName",
    title: '所属云平台'
},{
    key: 'ip',
    dataIndex:"ip",
    title: '业务厂家'
},{
    key: 'principal',
    dataIndex:"principal",
    title: '所属硬件资源池'
},{
    key: 'relatedPool',
    dataIndex:"relatedPool",
    title: 'VPC'
},{
    key: 'principal',
    dataIndex:"principal",
    title: '入网时间'
},{
    key: 'relatedPool',
    dataIndex:"relatedPool",
    title: '业务系统等级'
},{
    key: 'relatedPool',
    dataIndex:"relatedPool",
    title: '刀片服务器'
},{
    key: 'relatedPool',
    dataIndex:"relatedPool",
    title: '小型机服务器'
},{
    key: 'relatedPool',
    dataIndex:"relatedPool",
    title: '关联虚拟机'
}]
export default function initTable(){
    return {
        columns
    }
}