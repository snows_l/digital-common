import { reactive } from 'vue';

import api from '@/api/networkAccess';

export function init() {
  const orderForm = reactive({
    userId: '',
    userName: '1',
    crateTime: '',
    id: '',
    point: '',
    pointName: '',
    group: null,
    cloud: null,
    relatedPool: null
  });
  let tableList = reactive({
    map: {}
  });
  function getUserInfo() {
    api.getUserInfo().then(({ data: { data } }) => {
      orderForm.userId = data?.user.id;
      orderForm.userName = data?.user?.username;
    });
  }
  function findDraft() {
    api.findDraft().then(({ data: { data } }) => {
      if (!data) return;
      Object.keys(orderForm).forEach(key => {
        orderForm[key] = data[key];
      });
      data['deviceGroupLists'].forEach(t => {
        let columns = t.deviceAttrs.attr.map(t => {
          return {
            key: t.name,
            dataIndex: t.name,
            width: '200px',
            title: t.property
          };
        });
        columns.push({
          title: '操作',
          key: 'operationDelete',
          fixed: 'right',
          width: 160
        });
        console.log('operationDelete', columns);
        tableList.map[t.deviceTypeCode] = {
          name: t.deviceType,
          columns,
          list: t.deviceInfoList.map(t => {
            return {
              ...t,
              key: t.instance_id
            };
          })
        };
      });
    });
  }
  function init() {
    getUserInfo();
    findDraft();
  }
  init();
  const cloudOptions = [
    {
      value: '网络云大区',
      label: '网络云大区'
    },
    {
      value: '网络云省内',
      label: '网络云省内'
    },
    {
      value: '网络边缘云',
      label: '网络边缘云'
    },
    {
      value: '移动边缘云',
      label: '移动边缘云'
    },
    {
      value: '融合边缘云',
      label: '融合边缘云'
    },
    {
      value: '平台云',
      label: '平台云'
    },
    {
      value: '政企云',
      label: '政企云'
    },
    {
      value: '非云',
      label: '非云'
    }
  ];

  let poolOptions = reactive({
    list: []
  });

  const groupOptions = [
    {
      value: '网络组',
      label: '网络组'
    },
    {
      value: '存储组',
      label: '存储组'
    },
    {
      value: '主机组',
      label: '主机组'
    },
    {
      value: '数据库组',
      label: '数据库组'
    }
  ];
  function getPoolByCloud(cloud) {
    api.getPoolByCloud(cloud).then(res => {
      console.log(res);
      poolOptions.list = res.data.data.map(t => {
        return {
          value: t,
          label: t
        };
      });
    });
  }

  return {
    orderForm,
    tableList,
    cloudOptions,
    poolOptions,
    getPoolByCloud,
    groupOptions
  };
}
