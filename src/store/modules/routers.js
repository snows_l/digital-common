/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-12-27 17:14:50
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-12-28 11:34:46
 * @FilePath: /digital-common/src/store/modules/routers.js
 */
const useActionRouterStore = defineStore('useActionRouterStore', {
  state: () => ({
    dynamicRouter: [],
    writeList: []
  }),

  actions: {
    // 存储动态router方法
    setRouterData(newVal) {
      this.dynamicRouter = newVal;
    },

    setWriteList(newVal) {
      this.writeList = newVal;
    }
  },
  persist: true
});

export default useActionRouterStore;
