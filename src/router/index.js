import { createRouter, createWebHashHistory } from 'vue-router';
/* Layout */
import Layout from '@/layout';

/**
 * Note: 路由配置项
 *
 * hidden: true                     // 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true                 // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 *                                  // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 *                                  // 若你想不管路由下面的 children 声明的个数都显示你的根路由
 *                                  // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect             // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'               // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * query: '{"id": 1, "name": "ry"}' // 访问路由的默认传递参数
 * roles: ['admin', 'common']       // 访问路由的角色权限
 * permissions: ['a:a:a', 'b:b:b']  // 访问路由的菜单权限
 * meta : {
    noCache: true                   // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
    title: 'title'                  // 设置该路由在侧边栏和面包屑中展示的名字
    icon: 'svg-name'                // 设置该路由的图标，对应路径src/assets/icons/svg
    breadcrumb: false               // 如果设置为false，则不会在breadcrumb面包屑中显示
    activeMenu: '/system/user'      // 当路由设置了该属性，则会高亮相对应的侧边栏。
  }
 */

// 公共路由
export const constantRoutes = [
  {
    path: '/',
    redirect: '/login',
    hidden: true
  },
  {
    path: '/login',
    component: () => import('@/views/login'),
    hidden: true
  },
  {
    path: '/totalView',
    component: Layout,
    children: [
      {
        path: '',
        component: () => import('@/views/totalView')
      }
    ]
  },
  {
    path: '/totalView3',
    component: Layout,
    children: [
      {
        path: '',
        component: () => import('@/views/totalViewhome')
      }
    ]
  },
  {
    path: '/editPassword',
    component: () => import('@/views/editPassword')
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/403',
    component: () => import('@/views/403'),
    hidden: true
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/404',
    hidden: true
  }
];

// 质量运维
export const opsRoutes = [
  // {
  //   path: '/operation',
  //   component: Layout,
  //   redirect: '/operation/overview',
  //   children: [
  //     {
  //       path: 'overview',
  //       name: 'Overview',
  //       component: () => import('@/views/overview/index'),
  //       meta: { title: '概览', icon: '概览 ' }
  //     }
  //   ]
  // },
];

// 资源
export const oprRoutes = [
  {
    path: '/operations',
    component: Layout,
    redirect: { name: 'resourceSearch' },
    meta: { title: 'CMDB资源查询', icon: '资源检索' },
    children: [
      {
        path: 'resourceSearch',
        name: 'resourceSearch',
        component: () => import('@/views/resourceSearch/index'),
        meta: { title: 'CMDB资源查询', icon: '资源检索' }
      },
      {
        name: 'searchResult',
        path: 'searchResult',
        component: () => import('@/views/advice/index')
      },
      {
        name: 'details',
        path: 'details',
        component: () => import('@/views/advice/details.vue')
      }
    ]
  },
  {
    path: '/operations',
    redirect: { name: 'machineRoom' },
    component: Layout,
    // meta: { title: '多云现场设备管理', icon: '资源检索' },
    children: [
      {
        path: 'machineRoom',
        name: 'MachineRoom',
        component: () => import('@/views/resource/index'),
        meta: { title: '多云现场设备管理', icon: '资源检索' }
      },
      {
        path: 'hardware',
        name: 'hardware',
        component: () => import('@/views/resource/index'),
        meta: { title: '资源池视图' }
      }
    ]
  },
  {
    path: '/operations/projectAccess',
    name: 'ProcessManagement',
    meta: {
      title: 'CMDB数据管理流程',
      icon: '流程管理',
      roles: ['工程组', '超级管理员']
    },
    component: Layout,
    alwaysShow: true,
    children: [
      {
        path: 'projectAccess',
        name: 'ProjectAccess',
        meta: { title: '工程入网' },
        component: () => import('@/views/processManagement/projectAccess')
      },
      {
        path: 'networkElementTwo',
        name: 'NetworkElementTwo',
        meta: { title: '实体网元下线' },
        redirect: { name: 'apply' },
        component: () => import('@/views/processManagement/networkAccess/index'),
        children: [
          {
            name: 'apply',
            path: 'apply',
            component: () => import('@/views/processManagement/networkAccess/apply')
          },
          {
            name: 'resRelation',
            path: 'resRelation/:orderId',
            component: () => import('@/views/processManagement/networkAccess/resRelation')
          },
          {
            name: 'professionalConfirm1',
            path: 'professionalConfirm1/:orderId',
            component: () => import('@/views/processManagement/networkAccess/professionalConfirm1')
          },
          {
            name: 'generalControlConfirm',
            path: 'generalControlConfirm/:orderId',
            component: () => import('@/views/processManagement/networkAccess/generalControlConfirm')
          },
          {
            name: 'professionalConfirm2',
            path: 'professionalConfirm2/:orderId',
            component: () => import('@/views/processManagement/networkAccess/professionalConfirm2')
          },
          {
            name: 'dataConfirm',
            path: 'dataConfirm/:orderId',
            component: () => import('@/views/processManagement/networkAccess/dataConfirm')
          },
          {
            name: 'complete',
            path: 'complete/:orderId',
            component: () => import('@/views/processManagement/networkAccess/complete')
          }
        ]
      },
      {
        path: 'flowSecondIPNetworkAccess',
        name: 'FLowSecondIPNetworkAccess',
        meta: { title: '二级IP地址入网' },
        redirect: 'index',
        component: () => import('@/views/processManagement/flowSecondIPNetworkAccess/view'),
        children: [
          {
            path: 'index',
            name: 'FLowSecondIPNetworkAccess',
            meta: { title: '二级IP地址入网' },
            component: () => import('@/views/processManagement/flowSecondIPNetworkAccess/index')
          },
          {
            path: 'add',
            name: 'FLowSecondIPNetworkAccessAdd',
            meta: { title: '新增' },
            component: () => import('@/views/processManagement/flowSecondIPNetworkAccess/add')
          },
          {
            path: 'finish-order',
            name: 'FLowSecondIPNetworkAccessFinishOrder',
            meta: { title: '新增' },
            component: () => import('@/views/processManagement/flowSecondIPNetworkAccess/finishOrder')
          },
          {
            path: 'cancel-order',
            name: 'FLowSecondIPNetworkAccessCancelOrder',
            meta: { title: '新增' },
            component: () => import('@/views/processManagement/flowSecondIPNetworkAccess/cancelOrder')
          }
        ]
      },
      {
        path: 'flowSecondVLANNetworkAccess',
        name: 'FlowSecondVLANNetworkAccess',
        redirect: 'index',
        meta: { title: '二级VLAN入网' },
        component: () => import('@/views/processManagement/flowSecondVLANNetworkAccess/view'),
        children: [
          {
            path: 'index',
            name: 'FLowSecondVIANNetworkAccessIndex',
            meta: { title: '二级VLAN入网' },
            component: () => import('@/views/processManagement/flowSecondVLANNetworkAccess/index')
          },
          {
            path: 'add',
            name: 'FlowSecondVLANNetworkAccessAdd',
            meta: { title: '二级VLAN入网' },
            component: () => import('@/views/processManagement/flowSecondVLANNetworkAccess/add')
          },
          {
            path: 'finish-order',
            name: 'FLowSecondVIANNetworkAccessFinishOrder',
            meta: { title: '二级VLAN入网' },
            component: () => import('@/views/processManagement/flowSecondVLANNetworkAccess/finishOrder')
          },
          {
            path: 'cancel-order',
            name: 'FLowSecondVIANNetworkAccessCancelOrder',
            meta: { title: '二级VLAN入网' },
            component: () => import('@/views/processManagement/flowSecondVLANNetworkAccess/cancelOrder')
          }
        ]
      }
    ]
  },
  {
    path: '/operations/workManagement',
    redirect: '/operations/workManagement/workManagement',
    meta: { title: '工单管理', icon: '工单管理' },
    name: 'workManagement',
    component: Layout,
    alwaysShow: true,
    children: [
      {
        path: 'workManagement',
        name: 'WorkManagement',
        meta: { title: '工程入网工单' },
        component: () => import('@/views/workManagement/index')
      },
      {
        path: 'networkElement',
        name: 'NetworkElement',
        meta: { title: '实体网元下线' },
        component: () => import('@/views/workManagement/networkElement/index')
      },
      {
        path: 'orderSecondIPNetworkAccess',
        name: 'OrderSecondIPNetworkAccess',
        meta: { title: '二级IP地址入网' },
        component: () => import('@/views/workManagement/orderSecondIPNetworkAccess/index')
      },
      {
        path: 'orderSecondVLANNetworkAccess',
        name: 'OrderSecondVLANNetworkAccess',
        meta: { title: '二级VLAN入网' },
        component: () => import('@/views/workManagement/orderSecondVLANNetworkAccess/index')
      }
    ]
  }
];

// 运维工作台
export const stagingRoutes = [
  // {
  //   path: '/operation/opsPlatform',
  //   redirect: '/operation/opsPlatform/overView',
  //   component: Layout,
  //   meta: { title: '运维工作台', icon: '概览' },
  //   children: [
  //     {
  //       path: 'overView',
  //       name: 'overView',
  //       meta: { title: '运维看板' },
  //       component: () => import('@/views/opsPlatform/overView')
  //     },
  //     {
  //       path: 'storageCapacityQuery',
  //       name: 'storageCapacityQuery',
  //       meta: { title: '存储容量查询' },
  //       component: () => import('@/views/opsPlatform/storageCapacityQuery')
  //     },
  //     {
  //       path: 'storageOffLineResolve',
  //       name: 'storageOffLineResolve',
  //       component: () => import('@/views/opsPlatform/storageOffLineResolve'),
  //       meta: { title: '存储断连处置' }
  //     }
  //   ]
  // }
];

// 系统设置
export const sysRoutes = [
  {
    path: '/operation/systemManagement',
    redirect: '/operation/systemManagement/authorComponents',
    component: Layout,
    meta: { title: '系统管理', icon: '权限管理' },
    roles: ['admin'],
    children: [
      {
        path: 'authorComponents',
        name: 'AuthorComponents',
        meta: { title: '权限管理' },
        component: () => import('@/views/systemManagement/authorComponents')
      },
      {
        path: 'userManagement',
        name: 'UserManagement',
        meta: { title: '用户管理' },
        component: () => import('@/views/systemManagement/userManagement')
      },
      {
        path: 'roleManagement',
        name: 'RoleManagement',
        meta: { title: '角色管理' },
        component: () => import('@/views/systemManagement/roleManagement')
      }
    ]
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes: [...constantRoutes, ...oprRoutes, ...opsRoutes, ...stagingRoutes, ...sysRoutes],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { top: 0 };
    }
  }
});
export default router;
