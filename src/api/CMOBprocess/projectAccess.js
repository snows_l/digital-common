import request from '@/utils/request'

// 下拉框选择值
export function selectList(cloudPool) {
    return request.get(`/api/cmdb_order/enum_field?pool=${cloudPool}`)
}

// 工程组提交申请
export function submitApply(data) {
    return request.post('/api/cmdb_order/add_order',data)
}
// 工程组待办列表
export function engineeringAgent(data) {
    return request.post('/api/cmdb_order/project_unfinished',data)
}
// 工程组已办列表
export function engineeringDone(data) {
    return request.post('/api/cmdb_order/project_finished',data)
}
// 工程组详情
export function engineeringDetail(orderId) {
    return request.get(`/api/cmdb_order/details_order/${orderId}`)
}
// 工程组取消工单
export function cancelOrder(data) {
    return request.put('/api/cmdb_order/cancel_order',data)
}
// 工程组提交被驳回工单
export function againSubmitOrder(orderId,data) {
    return request.put(`/api/cmdb_order/edit_order/${orderId}`,data)
}
// 工程组上传文件
export function uploadFile(data) {
    let formData = new FormData()
    let keys = Object.keys(data)
    for (let i = 0; i < keys.length; i++) {
        let key = keys[i]
        formData.append(key,data[key])
    }
    return request.post('/api/cmdb_order/audit_excel',formData)
}
// 工程组模板下载
export function downloadTemp(hasdata) {
    return request.get(`/api/cmdb_order/download_template?device=${hasdata}`,{ responseType: 'blob'})
}
// 工单下载
export function downloadOrder(hasvalue) {
    return request.get(`/api/cmdb_order/filedown/${hasvalue}`,{ responseType: 'blob'})
}

// 专业组待办列表
export function specialtyAgent(data) {
    return request.post('/api/cmdb_order/specialty_unfinished',data)
}
// 专业组已办
export function specialtyDone(data) {
    return request.post('/api/cmdb_order/specialty_finished',data)
}
// 专业组驳回工单
export function specialtyRejectOrder(orderId,data) {
    return request.put(`/api/cmdb_order/specialty_order_reject/${orderId}`,data)
}
// 专业组审核通过
export function specialtyPassOrder(orderId,data) {
    return request.put(`/api/cmdb_order/specialty_order_pass/${orderId}`,data)
}

// 数据组代办列表
export function dataAgent(data) {
    return request.post('/api/cmdb_order/data_unfinished',data)
}
// 数据组已办列表
export function dataDone(data) {
    return request.post('/api/cmdb_order/data_finished',data)
}
// 数据组审核通过
export function dataPassOrder(orderId,data) {
    return request.put(`/api/cmdb_order/data_pass/${orderId}`,data)
}
// 数据组数据更新
export function dataUpdate(data) {
    return request.post('/api/cmdb_order/csv_input_cmdb',data)
}

// CRE组代办列表
export function creAgent(data) {
    return request.post('/api/cmdb_order/cre_unfinished',data)
}
// CRE组已办列表
export function creDone(data) {
    return request.post('/api/cmdb_order/cre_finished',data)
}
// CRE组审核通过
export function crePassOrder(orderId,data) {
    return request.put(`/api/cmdb_order/cre_pass/${orderId}`,data)
}
