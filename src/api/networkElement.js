/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-10-07 14:25:45
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2024-01-08 15:36:46
 * @FilePath: /digital-common/src/api/networkElement.js
 */
import request from '@/utils/request2';

// 获取待办列表
export const getList = data => {
  return request({
    url: '/shuoang/api/ipNetin/list',
    method: 'post',
    data
  });
};

// 获取待办列表
export const getCommissionList = data => {
  return request({
    url: '/shuoang/api/ipNetin/getCommissionList',
    method: 'post',
    data
  });
};

// 获取已办列表
export const getDoneList = data => {
  return request({
    url: '/shuoang/api/ipNetin/getDoneList',
    method: 'post',
    data
  });
};

// 获取工单详情
export const getWorkerOrderDetail = id => {
  return request({
    url: `/shuoang/api/ipNetin/getOrderInfo/${id}`,
    method: 'get'
  });
};

// 获取当前登录用户信息
export const getUserInfo = () => {
  return request({
    url: `/shuoang/api/ipNetin/getUserInfo`,
    method: 'post'
  });
};

// 获取资源池
export const getPoolByCloud = data => {
  return request({
    url: `/shuoang/api/ipNetin/getPoolByCloud`,
    method: 'post',
    data
  });
};

// 取消工单
export const cancelWorkerOrder = id => {
  return request({
    url: `/shuoang/api/ipNetin/cancel/${id}`,
    method: 'get'
  });
};

// 发起工单
export const submitOrder = data => {
  return request({
    url: `/shuoang/api/ipNetin/submitOrder`,
    method: 'post',
    data
  });
};

// 查询责任人
export const getOwner = data => {
  return request({
    url: `/shuoang/api/ipNetin/getOwner`,
    method: 'post',
    data
  });
};
