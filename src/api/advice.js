/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-12-20 10:08:29
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2024-01-11 11:10:20
 * @FilePath: /digital-common/src/api/advice.js
 */
import request from '@/utils/request2';
//全局检索
export const fullSearch = data => {
  return request({
    url: '/shuoang/api/resourceQuery/fullSearch',
    method: 'post',
    data
  });
};

export const findInfo = data => {
  return request({
    url: '/shuoang/api/resourceQuery/info',
    method: 'post',
    data
  });
};

export default {
  fullSearch,
  findInfo
};
