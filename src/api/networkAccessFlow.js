/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-10-07 14:25:45
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-12-18 09:40:33
 * @FilePath: /digital-operation-platform-common/src/api/networkAccessFlow.js
 */
import request from "@/utils/request2";

// 获取待办列表
export const getList = (data) => {
  return request({
    url: "/shuoang/api/vlanNetin/list",
    method: "post",
    data,
  });
};

// 获取待办列表
export const getCommissionList = (data) => {
  return request({
    url: "/shuoang/api/vlanNetin/getCommissionList",
    method: "post",
    data,
  });
};

// 获取已办列表
export const getDoneList = (data) => {
  return request({
    url: "/shuoang/api/vlanNetin/getDoneList",
    method: "post",
    data,
  });
};

// 获取工单详情
export const getWorkerOrderDetail = (id) => {
  return request({
    url: `/shuoang/api/vlanNetin/getOrderInfo/${id}`,
    method: "get",
  });
};

// 获取当前登录用户信息
export const getUserInfo = () => {
  return request({
    url: `/shuoang/api/vlanNetin/getUserInfo`,
    method: "post",
  });
};

// 获取资源池
export const getPoolByCloud = (data) => {
  return request({
    url: `/shuoang/api/vlanNetin/getPoolByCloud`,
    method: "post",
    data,
  });
};

// 取消工单
export const cancelWorkerOrder = (id) => {
  return request({
    url: `/shuoang/api/vlanNetin/cancel/${id}`,
    method: "get",
  });
};

// 发起工单
export const submitOrder = (data) => {
  return request({
    url: `/shuoang/api/vlanNetin/submitOrder`,
    method: "post",
    data,
  });
};

// 查询责任人
export const getOwner = (data) => {
  return request({
    url: `/shuoang/api/vlanNetin/getOwner`,
    method: "post",
    data,
  });
};
