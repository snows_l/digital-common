import request from '@/utils/request2';
//查询机房
const findRoomByCloud = data => {
  return request({
    url: '/shuoang/api/device/findRoomByCloud',
    method: 'post',
    data
  });
};

const findRoomByCardCloud = data => {
  return request({
    url: '/shuoang/api/fieldDevice/findRoomByCloud',
    method: 'post',
    data
  });
};

const findMainData = data => {
  return request({
    url: '/shuoang/api/fieldDevice/findMainData',
    method: 'post',
    data
  });
};

// 查询机柜列表
const findRackByRoom = data => {
  return request({
    url: '/shuoang/api/fieldDevice/findRackByRoom',
    method: 'post',
    data
  });
};

const findDeviceByRack = data => {
  return request({
    url: `/shuoang/api/fieldDevice/findDeviceByRack`,
    method: 'post',
    data
  });
};

// 查询社保想去
const findDeviceDisById = id => {
  return request({
    url: `/shuoang/api/device/info/${id}`,
    method: 'get'
  });
};

const findBussinessById = id => {
  return request({
    url: `/shuoang/api/device/bussiness/${id}`,
    method: 'get'
  });
};

// 全局搜索设备列表
const findFieldDeviceList = data => {
  return request({
    url: '/shuoang/api/fieldDevice/queryList',
    method: 'post',
    data
  });
};

// 全局搜索设备列表
const findDeviceList = data => {
  return request({
    url: '/shuoang/api/device/queryList',
    method: 'post',
    data
  });
};

const findFieldDeviceListAdvanced = data => {
  return request({
    url: '/shuoang/api/fieldDevice/queryListAdvanced',
    method: 'post',
    data
  });
};

const findDeviceListAdvanced = data => {
  return request({
    url: '/shuoang/api/device/queryListAdvanced',
    method: 'post',
    data
  });
};

function findRelatedPool(data) {
  return request({
    url: `/shuoang/api/device/relatedPool`,
    method: 'post',
    data
  });
}

function findDeviceTypeAll(data) {
  return request({
    url: `/shuoang/api/device/deviceType/byPool`,
    method: 'post',
    data
  });
}

function findByRelatedPool(data) {
  return request({
    url: `/shuoang/api/device/findByRelatedPool`,
    method: 'POST',
    data
  });
}

export default {
  findRoomByCloud,
  findRoomByCardCloud,
  findMainData,
  findRackByRoom,
  findDeviceByRack,
  findDeviceDisById,
  findBussinessById,
  findFieldDeviceList,
  findDeviceList,
  findFieldDeviceListAdvanced,
  findDeviceListAdvanced,
  findRelatedPool,
  findDeviceTypeAll,
  findByRelatedPool
};
