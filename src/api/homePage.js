/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-12-18 16:52:19
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-12-18 16:52:28
 * @FilePath: /digital-operation-platform-common/src/api/homePage.js
 */
import request from '@/utils/request2';
//获取分组模型
const findTypeByGroup = () => {
  return request({
    url: '/shuoang/api/resourceQuery/typeByGroup',
    method: 'get'
  });
};
//模糊搜索全局
const findDataViolence = data => {
  return request({
    url: '/shuoang/api/resourceQuery/search',
    method: 'post',
    data
  });
};

//获取所有字段
const findAllAttr = typeCode => {
  return request({
    url: `/shuoang/api/resourceQuery/attrByType/${typeCode}`
  });
};
//高级搜索
const advancedSearchTable = data => {
  return request({
    url: `/shuoang/api/resourceQuery/advancedSearch`,
    method: 'post',
    data
  });
};

export default {
  findTypeByGroup,
  findDataViolence,
  findAllAttr,
  advancedSearchTable
};
