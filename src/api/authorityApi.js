import request from '@/utils/request';

// 用户登录
export function userLogin(data) {
  return request.post('/api/auth/user/login', data);
}

export function userInfo() {
  return request({
    url: '/api/auth/user/info',
    method: 'get'
  });
}

// 用户列表
export function getUser(params) {
  return request({
    url: '/api/auth/user',
    method: 'get',
    params
  });
}

// 角色列表
export function getRole(params) {
  return request({ url: '/api/auth/role', method: 'get', params });
}

// 新增用户
export function addUser(data) {
  return request.post('/api/auth/user/register', data);
}

// 修改用户信息
export function editUser(userId, data) {
  return request.put(`/api/auth/user/${userId}`, data);
}

// 删除用户
export function delUser(userId) {
  return request.delete(`/api/auth/user/${userId}`);
}

// 重置密码
export function resetPsw(userId) {
  return request.get(`/api/auth/user/password/reset/${userId}`);
}

// 修改密码
export function editPassword(data) {
  return request.put('/api/auth/user/password', data);
}

// 新增角色
export function addRole(data) {
  return request.post('/api/auth/role', data);
}

// 修改角色信息
export function editRole(roleId, data) {
  return request.put(`/api/auth/role/${roleId}`, data);
}

// 获取角色列表
export function RoleTable(params) {
  return request({
    url: '/api/auth/role',
    method: 'get',
    params
  });
}

// 修改角色权限
export function editRoleAuthority(roleId, data) {
  return request.put(`/api/auth/role/permission/${roleId}`, data);
}

// 创建权限
export function addAuthority(data) {
  return request.post('/api/auth/permission', data);
}

// 获取权限列表
export function authorityTable(roleId) {
  return request.get(`/api/auth/permission/${roleId}`);
}

// 删除权限模块
export function delAuthority(permissionId) {
  return request.delete(`/api/auth/permission/${permissionId}`);
}

// 修改目录模块
export function editAuthority(permissionId, data) {
  return request.put(`/api/auth/permission/${permissionId}`, data);
}

// 删除角色
export function delRole(roleId) {
  return request.delete(`/api/auth/role/${roleId}`);
}
