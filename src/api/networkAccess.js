import request from '@/utils/request2';
//查询机房
const findRoomByCloud = data => {
  return request({
    url: '/shuoang/api/device/findRoomByCloud',
    method: 'post',
    data
  });
};

// 获取云
const getPoolByCloud = cloud => {
  return request({
    url: '/shuoang/api/downline/getPoolByCloud',
    method: 'post',
    data: {
      cloud
    }
  });
};
//获取当前用户信息
const getUserInfo = () => {
  return request({
    url: '/shuoang/api/downline/getUserInfo',
    method: 'post'
  });
};
//获取设备类型列表
const getTypeByGroup = data => {
  return request({
    url: '/shuoang/api/downline/getTypeByGroup',
    method: 'post',
    data
  });
};

// 获取机房列表
const getRoom = data => {
  return request({
    url: '/shuoang/api/downline/getRoom',
    method: 'post',
    data
  });
};

//查询设备列表
const findDevice = data => {
  return request({
    url: '/shuoang/api/downline/findDevice',
    method: 'post',
    data
  });
};

// 获取草稿数据
const findDraft = () => {
  return request({
    url: '/shuoang/api/downline/findDraft',
    method: 'post'
  });
};

// 发起工单
const submitOrder = data => {
  return request({
    url: '/shuoang/api/downline/submitOrder',
    method: 'post',
    data
  });
};

// 获取工单详情
const getOrderInfo = id => {
  return request({
    url: `/shuoang/api/downline/getOrderInfo/${id}`,
    method: 'get'
  });
};

// 刷新实例关系
const refreshDevice = id => {
  return request({
    url: `/shuoang/api/downline/refreshDevice/${id}`,
    method: 'get'
  });
};
// 数据更新
const refresh = id => {
  return request({
    url: `/shuoang/api/downline/refresh/${id}`,
    method: 'get'
  });
};
// 再次提交工单
const checkOrder = id => {
  return request({
    url: `/shuoang/api/downline/checkOrder/${id}`,
    method: 'get'
  });
};

// 总控机确认
const checkOrderByZk = id => {
  return request({
    url: `/shuoang/api/downline/checkOrderByZk/${id}`,
    method: 'get'
  });
};

// 补充设备退网时间
const replenish = (id, data) => {
  return request({
    url: `/shuoang/api/downline/replenish/${id}`,
    method: 'post',
    data
  });
};

// 数据组-确认
const checkOrderBySj = id => {
  return request({
    url: `/shuoang/api/downline/checkOrderBySj/${id}`,
    method: 'get'
  });
};
export default {
  findRoomByCloud,
  getPoolByCloud,
  getUserInfo,
  getTypeByGroup,
  getRoom,
  findDevice,
  findDraft,
  submitOrder,
  getOrderInfo,
  refreshDevice,
  refresh,
  checkOrder,
  checkOrderByZk,
  replenish,
  checkOrderBySj
};
