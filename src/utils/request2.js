/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-12-13 17:09:31
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2024-01-11 10:02:48
 * @FilePath: /digital-common/src/utils/request2.js
 */
import axios from 'axios';

import { ElMessage } from 'element-plus';

// create an axios instance
const service = axios.create({
  baseURL: process.env.NODE_ENV === 'development' ? '' : null,
  timeout: 80000 // request timeout
});

// request interceptor
service.interceptors.request.use(
  config => {
    if (sessionStorage.getItem('token')) {
      config.headers['x-token'] = sessionStorage.getItem('token');
    }

    if (sessionStorage.getItem('local_login')) {
      config.headers['x-local'] = sessionStorage.getItem('local_login') == 'true' ? true : false;
    }

    return config;
  },
  error => {
    console.log(error); // for debug
    return Promise.reject(error);
  }
);

// 避免报错的时候 多个接口一次性报错 弹出多个报错框影响观感
let errorELMessage = () => {
  let prevTime;
  return function (msg) {
    let currTime = Date.now();
    if (!prevTime) {
      prevTime = currTime;
      ElMessage.error(msg);
    } else {
      if (currTime - prevTime > 800) {
        ElMessage.error(msg);
        prevTime = Date.now();
      }
    }
  };
};
let showErrorELMessage = errorELMessage();

service.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    showErrorELMessage(error);
    return Promise.reject(error);
  }
);
export default service;
