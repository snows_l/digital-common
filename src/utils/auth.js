/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-12-13 11:18:51
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2024-01-05 15:34:30
 * @FilePath: /digital-common/src/utils/auth.js
 */
import Cookies from 'js-cookie';

const TokenKey = 'Admin-Token';

export function getToken() {
  return Cookies.get(TokenKey);
}

export function setToken(token) {
  return Cookies.set(TokenKey, token);
}

export function removeToken() {
  return Cookies.remove(TokenKey);
}

export function logout() {
  localStorage.clear();
  sessionStorage.clear();
}
