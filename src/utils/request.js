/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-12-08 15:48:47
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2024-01-11 10:03:36
 * @FilePath: /digital-common/src/utils/request.js
 */
import router from '@/router';
import { logout } from '@/utils/auth';
import axios from 'axios';
import { ElMessage } from 'element-plus';

const service = axios.create({
  baseURL: process.env.NODE_ENV === 'development' ? '/apps' : null,
  timeout: 80000 // request timeout
});

// request interceptor
service.interceptors.request.use(
  config => {
    if (sessionStorage.getItem('token')) {
      config.headers.common['x-token'] = sessionStorage.getItem('token');
    }

    // 缓存控制
    if (config.CacheControl) {
      config.headers['Cache-Control'] = config.CacheControl;
    }

    config.headers.common['local-login'] = sessionStorage.getItem('local_login') == 'true' ? true : false;
    return config;
  },
  error => {
    console.log(error); // for debug
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    // ElMessage.error(error);
    if (error.response.status == 401) {
      ElMessage.error('登陆已过期， 请重新登陆！');
      logout();
      router.push('/');
    }
    return Promise.reject(error);
  }
);
export default service;
