/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-12-13 11:18:51
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2024-01-12 17:21:20
 * @FilePath: /digital-common/src/permission.js
 */
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import router from './router';
// import { isRelogin } from '@/utils/request'

NProgress.configure({ showSpinner: false });

const whiteList = ['/login', '/register'];

router.beforeEach((to, from, next) => {
  NProgress.start();
  if (to.query.token) {
    localStorage.setItem('TotalViewName', 'home');
    window.sessionStorage.setItem('token', to.query.token);
    window.sessionStorage.setItem('local_login', to.query.local_login == 'true' ? true : false);
    router.push('/totalView');
    return;
  } else if (to.path === '/login') {
    if (sessionStorage.getItem('token')) {
      next('/totalView');
    } else {
      next();
    }
  } else if (!window.sessionStorage.getItem('token')) {
    next('/login');
  } else {
    next();
  }
});

router.afterEach(() => {
  NProgress.done();
});
